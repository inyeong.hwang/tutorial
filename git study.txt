-Git: 분산형 버전관리 시스템
기존 파일 버전 관리 -> 파일명/날짜,버전명 번거롭, 공유폴더 경우 동시 편집으로 인한 문제
Git 사용 -> 업데이트 이력이 Git에 저장되, 매번 파일 복사본을 만들 필요x
비슷한 툴: SVN

-Git 목적
1.버전/ 2.백업/ 3.협업

-Git 특징
1. 소스 코드 변경 이력 확인 가능
2. 특정 시점 버전과 비교 가능
3. 특정 시점 버전으로 되돌아가기 가능
4. 내가 변경한 파일이 다른 사람이 변경한 내용과 충돌하면, 업로드시 경고메세지 알림/ 버전 충돌 문제 해결 가능
5. 다른 팀원과 공유 가능
6. 배포할 때 유용하게 사용

-저장소(repository)
말 그대로 파일이나 폴더를 저장해 두는 곳
변경이력별로 구분되어 저장됨

-로컬 레포지토리 생성 방법
1. 로컬에 디렉토리 생성 후, 원격 레포지토리와 연결
2. 로컬에 디렉토리 생성 후, 원격 레포지토리를 복사 

-커밋(commit)
커밋 시, 영문/숫자 40자리로 이루어진 커밋고유번호가 생성됨
커밋고유번호를 이용하여 커밋 구분, 선택
이후 git log를 통해 커밋 히스토리를 확인할때 특정 커밋 이전의 히스토리 확인 가능

*커밋 시, 메시지 입력 필수
다른 사람과 협업 시, 변경내용 요약/변경 이유 등을 입력하면 좋음

add: 로컬폴더(작업트리) -> staging영역(인덱스)
commit: staging영역(인덱스) -> 로컬 레포지토리
-그냥 commit하면 되지 왜 add를 거칠까?
-인덱스가 존재함으로써, 커밋에 필요없는 파일을 지정해 커밋에 포함시키지 않을 수 있음
(파일 4개중 수정완료된 파일 3개만 commit하는 경우)
-인덱스가 존재함으로써, 내가 원하는 특정 변경 사항만 커밋할 수 있음

-git 초기 설정
<git config --global user.name "inyeong">
<git config --global user.email "inyeong.hwang@seizedata.com">

-git 로컬 레포지토리 생성
1. 로컬에 디렉토리 생성(mkdir)
2. 디렉토리 내 <git init> 실행 -> .git 디렉토리 생성

-git status
로컬폴더와 staging영역 상태 확인

<git status>
untracked files  : staging영역에 add되지 않아 추적대상 파일이 아님

<git add .> : 모든 파일 staging영역에 등록
<git status>
Changes to be committed  : staging영역에 add되어 commit 준비된 파일임

<git commit -m "first commit">
<git status>
nothing to commit   : 로컬 레포지토리에 commit되어, 로컬폴더와 staging영역에 뭐가 없음

<git log>: 레포지토리의 커밋 히스토리를 최근순으로 출력
(커밋고유번호, 저자이름, 저자이메일, 커밋날짜, 커밋메시지 확인 가능)

git log -p : 모든 커밋의 이전 커밋과의 diff 결과 출력
git log --stat : 어떤 파일들이 변경되었고 얼마나 많은 라인이 추가/삭제 되었는지 확인
gitk툴 : 변경이력을 GUI에서 확인 가능

git diff 'commit고유번호1' 'commit고유번호2' : 두 커밋의 diff 결과 출력

push: 로컬 레포지토리 -> 원격 레포지토리 (업로드)
clone: 원격레포지토리 -> 로컬 레포지토리 복사(통째로 다운로드, 변경이력도 함께)
pull: 원격 레포지토리 -> 로컬 레포지토리 (최신 변경 이력만 다운로드)

<gitlab 원격 레포지토리 생성>
서버에 원격레포지토리 생성하는 경우: ~.git 디렉토리 생성, git init --bare ~.git

<원격레포지토리 공유>
1.원격 레포지토리 등록
git remote add <name> <원격레포지토리 url>
<git remote add origin https://gitlab.com/inyeong.hwang/tutorial.git>  : 원격레포지토리를 origin이라는 이름으로 등록, 앞으로 origin으로 사용

push나 pull 실행 시, 원격레포지토리명을 생략하면 origin이란 이름의 원격레포지토리를 사용하기 때문에
origin으로 등록하는 것이 일반적

git push <원격레포지토리주소> <브랜치명>
<git push -u origin master>  : -u 옵션을 사용하면 앞으로 브랜치명 생략 가능

<git log --branches --not --remotes=origin>  : 원격 분기에 없고 로컬 분기에만 존재하는 모든 커밋


2.원격 레포지토리 복사
복제 레포지토리를 생성할 경로에서 실행
git clone <원격레포지토리주소> <생성할폴더명>
<git clone https://gitlab.com/inyeong.hwang/tutorial.git tutorial2>

<레포지토리 내 파일 내용 변경, add, commit>
<git push>
clone시에는 원격레포지토리 주소와 브랜치 생략 가능



git pull <원격레포지토리주소> <브랜치명>
<git pull origin master>


<자동 통합>
나: pull  ->  다른 사람: commit/push  -> 나: commit/push (거부됨)
pull하여 다른 사람의 업데이트 이력을 merge해야 함

<수동 통합>
나와 다른 사람이 동일한 부분을 변경한 경우! 다른 사람의 파일을 덮어쓰기 하는 예외를 처리함
pull 실행 시, merge 충돌 발생
내가 나중에 push 했어도, 두 변경내용 중 어느 쪽을 저장할지 git이 판단할 수 없어서 충돌 발생
git은 만들어진 날짜순 ???
Merge conflict in sample.txt로 표시됨 -> sample.txt 확인 -> =====로 구분되어 동일변경내용 표시
( git이 =====로 구분해 표시 (기준 위: 로컬, 아래: 원격) )
직접 수정 후 커밋

<git log --graph --oneline>
: 커밋이력이 선으로 된 그림 형태로 표시됨 (--oneline은 커밋정보가 한줄로 표시되는 옵션)

<branch란>
어떤 프로젝트를 여러 사람이 작업(기능추가/버그수정 등)할 때, 서로 영향을 받지 않고 동시에 작업하기 위한 개념
각 브랜치를 다른 브랜치와 merge함으로써 하나의 브랜치로 다시 모은다
자신의 브랜치에서 변경 사항을 적용할 때마다 기록을 남기게 되어, 문제가 발생할 경우 원인을 찾기 쉬워짐

통합브랜치: 언제든지 배포할 수 있는 버전을 만드는 브랜치
토픽브랜치: 작업을 위한 브랜치

checkout명령어를 실행해, 특정 브랜치로 전환 가능 (HEAD가 가리키게 해서)
HEAD는 현재 브랜치의 가장 마지막 커밋을 가리킴

stash명령:  로컬폴더와(작업트리) staging영역(인덱스) 내 아직 커밋되지 않은 변경을 일시적으로 저장

<브랜치 통합>
1. merge 사용
2. rebase 사용






<commit 되돌리기>
1. git reset --hard "커밋고유번호":   로컬에서 push 전 commit을 잘못했을 때 (해당 commit이후 commit들을 삭제)
--hard: 깃 히스토리(로그)에서 완전 삭제 / -soft: staging영역으로 이동 / --mixed(default): 로컬 폴더로 이동
(git reflog로 다시 reset을 취소할 수 있다..,)

<reset>
'HEAD는 계속 현재 브랜치를 가리키고, 현재 브랜치를 이동시켜 가리키는 커밋을 바꿈'
-soft: HEAD가 가리키는 값만 바뀌고 staging영역과 로컬디렉토리는 그대로 
--mixed: HEAD가 가리키는 값이 바뀌고 staging영역도 HEAD가 가리키는 값으로 바뀜, 로컬디렉토리는 그대로
--hard: HEAD가 가리키는 값이 바뀌고 staging영역은 HEAD가 가리키는 값으로, 로컬디렉토리는 staging영역이 가리키는 값으로 바뀜
--hard는 다른 옵션들과 다르게 히스토리에서 커밋을 완전 삭제하기 때문에 reflog로도 되돌릴 수 없음

2. git revert "커밋고유번호":   깃 히스토리는 삭제되지 않고, 해당 commit을 취소하는 commit
git revert -n "커밋고유번호": 해당 commit을 취소하고, 취소된 변경사항은 staging영역으로 이동

3. git checkout "커밋고유번호"
git checkout HEAD^: ^갯수만큼 이전으로 이동(^^^=~3) / 깃 히스토리는 그대로, 파일만 변화

<checkout>
'HEAD자체가 다른 커밋이나 브랜치를 가리키도록 함'


<staging 되돌리기>
git reset HEAD <파일명>:  staging(add) 취소

<파일 수정사항 되돌리기>
git checkout <파일경로>: 변경된 파일 되돌리기/ 생성된 것 삭제x

<push 취소하기>
1.git reset을 이용해 원하는 커밋지점으로 되돌리기
2.다시 commit
3.강제 push (git push -f)



git fetch -> git merge = git pull

pull: 원격레포지토리의 commit을 가져와 병합됨, 어떤 내용이 병합되었는지 알기가 힘듬
fetch:  로컬레포지토리와 원격레포지토리의 최근 커밋 비교, 내용 차이 확인
<git fetch origin> : fetch를 확인할 수 있는 브랜치 내역 확인 (origin/main)

<git pull --rebase origin main>
로컬분기가 원격분기보다 뒤에 commit한 경우 사용

-Git은 디렉토리 자체를 추적하지 않고, 디렉토리 내 파일 추적

-git diff: 로컬 레포지토리와 staging 영역의 차이 출력 (add안한것들 확인, add하면 차이 없음)
-git diff HEAD: 로컬 레포지토리, staging 영역과 원격 레포지토리의 차이 출력
-git diff --staged: staging 영역과 원격 레포지토리의 차이 출력 (commit안한것들 확인, commit하면 차이 없음 )
 (=git diff --cached)

*Git은 폴더/파일명의 대소문자를 구분하지 않음
git config core.ignorecase true
내 Git에서만 설정되고, 팀원들과 협업중이라면 팀원의 Git도 설정 필요

*업로드 시 파일명이 길어 생기는 에러 "Filename too long" 해결
git config --system core.longpaths true

*파일명 변경 시
git mv oldName newName
파일이동명령어 mv를 사용하여 rename과 같은 효과를 주며, 이름 변경사항도 추적 가능
대소문자로 변경하는 경우 Invalid argument 에러 발생 -> 임시 파일명으로 한번 더 거쳐줌

*.gitognore 파일 포함 예시
로그파일이나 컴파일 코드 등의 임시 리소스
공유할 필요가 없는 로컬 설정 파일
로그인 암호나 키, 인증 파일 같은 보안 파일
-변경 내역이 추적되지 않음
-add 명령에 staging영역에 추가되지 않음
-git status 나 git diff에 검출되지 않음